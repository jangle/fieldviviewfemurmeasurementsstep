'''
MAP Client, a program to generate detailed musculoskeletal models for OpenSim.
    Copyright (C) 2012  University of Auckland
    
This file is part of MAP Client. (http://launchpad.net/mapclient)

    MAP Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MAP Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MAP Client.  If not, see <http://www.gnu.org/licenses/>..
'''
import os
import random
import string
from PySide import QtCore, QtGui
import numpy as np

from mapclient.mountpoints.workflowstep import WorkflowStepMountPoint
from mapclientplugins.fieldviviewfemurmeasurementsstep.fieldviviewfemurmeasurementsdata import StepState
from gias2.musculoskeletal import viewfemurmeasurements

class FieldViViewFemurMeasurementsStep(WorkflowStepMountPoint):
    '''
    fieldvi view model step displays a given fieldwork model 
    using the fieldvi widget
    '''
    
    def __init__(self, location):
        super(FieldViViewFemurMeasurementsStep, self).__init__('FieldVi Femur Measurement Viewer', location)
        self._category = 'Fieldwork Measurements'
        self._state = StepState()
        # self._icon = QtGui.QImage(':/zincmodelsource/images/zinc_model_icon.png')   # change this
        # self.addPort(('http://physiomeproject.org/workflow/1.0/rdf-schema#port', 'http://physiomeproject.org/workflow/1.0/rdf-schema#uses', 'ju#fieldviviewer'))
        # self.addPort(('http://physiomeproject.org/workflow/1.0/rdf-schema#port', 'http://physiomeproject.org/workflow/1.0/rdf-schema#uses', 'ju#femurmeasurements'))
        self.addPort(('http://physiomeproject.org/workflow/1.0/rdf-schema#port', 'http://physiomeproject.org/workflow/1.0/rdf-schema#uses', 'ju#fieldworkmodelandmeasurements'))
        # self.addPort(('http://physiomeproject.org/workflow/1.0/rdf-schema#port', 'http://physiomeproject.org/workflow/1.0/rdf-schema#provides', 'ju#fieldviviewer'))
        self._widget = None
        self._configured = True
        self._identifier = generateIdentifier()


    def configure(self):
        return self._configured
    
    def getIdentifier(self):
        return self._identifier
     
    def setIdentifier(self, identifier):
        self._identifier = identifier
     
    def serialize(self):
        '''There is nothing to be done here for this step.
        '''
        return ''
     
    def deserialize(self, string):
        '''There is nothing to be done here for this step.
        '''
        pass
 
    def execute(self, dataIn):
        print 'executing FieldViViewFemurMeasurementsStep' 
        M, G = (dataIn['measurements'], dataIn['model'])
        # if not self._widget:
        self._widget = viewfemurmeasurements.viewMeasurements(M, G, onCloseCallback=self._doneExecution)
        
        # self._setCurrentWidget(self._widget)
     
def getConfigFilename(identifier):
    return identifier + '.conf'

def generateIdentifier(char_set=string.ascii_uppercase + string.digits):
    return ''.join(random.sample(char_set*6,6))